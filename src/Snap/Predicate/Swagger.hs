{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies          #-}

module Snap.Predicate.Swagger
    ( Producer (..)
    , Consumer (..)
    , Code     (..)

    , produces
    , consumes
    , code
    , valueOf
    ) where

import Data.Predicate
import Data.Predicate.Descr
import Data.Predicate.Typeof

data Producer a = Producer Descr
data Consumer a = Consumer Descr
data Code       = Code     Descr

produces :: (Typeof a) => a -> Producer a
produces a = Producer $ DType (typeof a) Required ["Result"]

consumes :: (Typeof a) => a -> Consumer a
consumes a = Consumer $ DType (typeof a) Required ["Body"]

code :: Int -> String -> Code
code c m = Code $ DSymbol (show c) m Optional ["Code"]

valueOf :: a
valueOf = undefined

instance (Typeof a) => Predicate (Producer a) b where
    type FVal (Producer a) = ()
    type TVal (Producer a) = ()
    apply _ _ = T 0 ()

instance Show (Producer a) where
    show (Producer d) = "Producer: " ++ show d

instance Description (Producer a) where
    describe (Producer d) = DLabel "Producer" d

instance (Typeof a) => Predicate (Consumer a) b where
    type FVal (Consumer a) = ()
    type TVal (Consumer a) = ()
    apply _ _ = T 0 ()

instance Show (Consumer a) where
    show (Consumer d) = "Consumer: " ++ show d

instance Description (Consumer a) where
    describe (Consumer d) = DLabel "Consumer" d

instance Predicate Code b where
    type FVal Code = ()
    type TVal Code = ()
    apply _ _ = T 0 ()

instance Show Code where
    show (Code d) = "Code: " ++ show d

instance Description Code where
    describe (Code d) = DLabel "Code" d
