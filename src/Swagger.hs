{-# LANGUAGE BangPatterns      #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections     #-}

module Swagger
    ( Options   (..)
    , Header    (..)
    , Swagger   (..)
    , Api       (..)
    , Operation (..)
    , Parameter (..)
    , Model     (..)
    , Property  (..)
    , Item      (..)

    , swagger
    , defaultOptions
    ) where

import Control.Applicative
import Data.Aeson
import Data.Aeson.Types hiding (Options, defaultOptions)
import Data.ByteString (ByteString)
import Data.Predicate.Descr
import Data.Function
import Data.Maybe
import Data.Monoid
import Data.Text (Text)
import Snap.Route (RouteDescr (..))

import qualified Data.ByteString.Char8  as Bytes
import qualified Data.List              as List
import qualified Data.Text              as T
import qualified Data.Text.Encoding     as T

data Options = Options
    { optRename :: Text -> Text
    }

data Header = Header
    { apiVersion   :: !Text
    , basePath     :: !Text
    , resourcePath :: !Text
    , contentTypes :: [Text]
    }

data Swagger = Swagger
    { swgSwaggerVersion :: !Text
    , swgApiVersion     :: !Text
    , swgBasePath       :: !Text
    , swgResourcePath   :: !Text
    , swgProduces       :: [Text]
    , swgApis           :: [Api]
    , swgModels         :: [Model]
    } deriving (Eq, Show)

data Api = Api
    { apiPath       :: !Text
    , apiOperations :: [Operation]
    } deriving (Eq, Show)

data Operation = Operation
    { opMethod     :: !Text
    , opSummary    :: Maybe Text
    , opNotes      :: Maybe Text
    , opType       :: !Text
    , opNickname   :: !Text
    , opParameters :: [Parameter]
    , opResponses  :: [Response]
    } deriving (Eq, Show)

data Parameter = Parameter
    { parName        :: Maybe Text
    , parDescription :: Maybe Text
    , parRequired    :: !Bool
    , parType        :: !Text
    , parParamType   :: !Text
    } deriving (Eq, Show)

data Model = Model
    { modName       :: !Text
    , modId         :: !Text
    , modProperties :: [(Text, Property)]
    , modRequired   :: [Text]
    } deriving (Eq, Show)

data Property = Property
    { propType        :: Maybe Text
    , propRef         :: Maybe Text
    , propFormat      :: Maybe Text
    , propDescription :: Maybe Text
    , propItems       :: Maybe Item
    , propEnum        :: Maybe [Text]
    , propMinimum     :: Maybe Text
    , propMaximum     :: Maybe Text
    , propObligation  :: !Obligation
    } deriving (Eq, Show)

data Response = Response
    { respCode    :: !Int
    , respMessage :: !Text
    } deriving (Eq, Show)

data Item
    = ItemType !Text
    | ItemRef  !Text
    deriving (Eq, Show)

emptyProp :: Property
emptyProp = Property Nothing Nothing Nothing Nothing
                     Nothing Nothing Nothing Nothing
                     Required

defaultOptions :: Options
defaultOptions = Options id

swagger :: Header -> Options -> [RouteDescr] -> Swagger
swagger h opts rr = Swagger
    { swgSwaggerVersion = "1.2"
    , swgApiVersion     = apiVersion h
    , swgBasePath       = basePath h
    , swgResourcePath   = resourcePath h
    , swgProduces       = contentTypes h
    , swgApis           = map api (groupByPath rr)
    , swgModels         = models opts rr
    }
  where
    groupByPath = List.groupBy ((==) `on` routePath)

api :: [RouteDescr] -> Api
api r = Api
    { apiPath       = T.decodeLatin1 $ mapPath (routePath (head r))
    , apiOperations = map operation r
    }
  where
    mapPath = Bytes.intercalate "/" . map enclose . Bytes.split '/'
    enclose t
        | Bytes.null t        = t
        | Bytes.head t == ':' = "{" <> Bytes.tail t <> "}"
        | otherwise           = t

operation :: RouteDescr -> Operation
operation r = Operation
    { opMethod     = T.pack . show . routeMethod $ r
    , opSummary    = routeDoc r
    , opNotes      = Nothing
    , opType       = fromMaybe "void" (routeType r)
    , opNickname   = ""
    , opParameters = map (parameter (routePath r)) (paramsByType r)
    , opResponses  = responses (routeDescr r)
    }

responses :: Descr -> [Response]
responses d = catMaybes $ mapD toResponse d
  where
    toResponse (DLabel "Code" (DSymbol c m Optional _)) =
        Just $ Response (read c) (T.pack m)
    toResponse _ =
        Nothing

routeType :: RouteDescr -> Maybe Text
routeType r =
    case catMaybes $ mapD producerType (routeDescr r) of
        Type n _:_ -> Just (T.pack n)
        _          -> Nothing
  where
    producerType (DLabel "Producer" (DType t _ _)) = Just t
    producerType _                                 = Nothing

routeDoc :: RouteDescr -> Maybe Text
routeDoc r =
    case catMaybes $ mapD doc (routeDescr r) of
        d:_ -> Just (T.pack d)
        _   -> Nothing
  where
    doc (DDoc d) = Just d
    doc _        = Nothing

data PKind = KParam | KHeader | KBody deriving Show

data Param = Param
    { pType  :: Maybe Type
    , pName  :: Maybe String
    , pTags  :: [Tag]
    , pOblig :: Maybe Obligation
    , pDescr :: Descr
    } deriving Show

paramsByType :: RouteDescr -> [(PKind, Param)]
paramsByType rd =
    let d = routeDescr rd
        t = filter notEmpty $ mapD toParam d
    in List.foldl' expand [] t
  where
    expand acc p = acc <> concat
        [ [(KParam, p)  | "Param"  `elem` pTags p]
        , [(KHeader, p) | "Header" `elem` pTags p]
        , [(KBody, p)   | "Body"   `elem` pTags p]
        ]

    notEmpty (Param Nothing Nothing [] Nothing _) = False
    notEmpty _                                    = True

    toParam d@(DConst  n _ t g) = Param (Just t) (Just n) g Nothing d
    toParam d@(DSymbol n _ o g) = Param Nothing  (Just n) g (Just o) d
    toParam d@(DValue  n t o g) = Param (Just t) (Just n) g (Just o) d
    toParam d@(DType   t o g)   = Param (Just t) Nothing  g (Just o) d
    toParam (DLabel _ !d)       = toParam d
    toParam d                   = Param Nothing Nothing [] Nothing d

parameter :: ByteString -> (PKind, Param) -> Parameter
parameter path (pkind, p) = Parameter
    { parName        = T.pack <$> pName p
    , parDescription = T.pack <$> descr (pDescr p)
    , parRequired    = fromMaybe False ((Required ==) <$> pOblig p)
    , parType        = fromMaybe "String" (pType p >>= typeName)
    , parParamType   = paramType pkind
    }
  where
    paramType KHeader = "header"
    paramType KBody   = "body"
    paramType KParam  = if inPath then "path" else "query"

    inPath = fromMaybe "" ((":" <>) . Bytes.pack <$> pName p) `Bytes.isInfixOf` path

    descr (DSymbol _ d _ _) = Just d
    descr _                 = Nothing

models :: Options -> [RouteDescr] -> [Model]
models opts = concatMap (model opts) . List.nub . concatMap (types . routeDescr)
  where
    types = foldD (\t d -> maybe t (:t) (typ d)) []

model :: Options -> Type -> [Model]
model o (Type n (TDCon _ (TRecd t))) =
    let (props, mm)  = properties o t
        isRequired p = propObligation (snd p) == Required
    in Model
        { modName       = T.pack n
        , modId         = T.pack n
        , modProperties = props
        , modRequired   = map fst (filter isRequired props)
        } : mm
model _ _ = []

properties :: Options -> [(String, Type)] -> ([(Text, Property)], [Model])
properties o fields =
    List.foldl' (\(pp, mm) (n, t) -> case property o t of
           (Just p, m)  -> ((optRename o (T.pack n), p):pp, m ++ mm)
           (Nothing, m) -> (pp, m ++ mm))
        ([], [])
        fields

property :: Options -> Type -> (Maybe Property, [Model])
property _ (TName n)                      = (Just (emptyProp { propType = Just (mapName $ T.pack n) }), [])
property o (Type "Maybe" (TSum s))        =
    case maybeProperty o s of
        (Just p, m) -> (Just $ p { propObligation = Optional}, m)
        other       -> other
property _ (Type n (TSum s))              = (sumProperty n s, [])
property o t@(Type n (TDCon _ (TRecd _))) = (Just (refProperty n), model o t)
property _ (Type _ (TDCon _ (TProd t)))   = (prdProperty t, [])
property _ (Type "List" (TName "Char"))   = (Just (emptyProp { propType = Just "string" }), [])
property _ (Type "List" t)                = (Just (collProperty t), [])
property _ _                              = (Nothing, [])

maybeProperty :: Options -> [Type] -> (Maybe Property, [Model])
maybeProperty o tc = case List.find just tc of
    Just (TDCon _ (TProd [t])) -> property o t
    _                          -> (Nothing, [])
  where
    just (TDCon "Just" (TProd [_])) = True
    just _                          = False

sumProperty :: String -> [Type] -> Maybe Property
sumProperty _ [] = Nothing
sumProperty n tc
    | all isNullary tc =
        let cn = map conName tc
        in Just $ emptyProp
            { propType = Just (T.pack n)
            , propEnum = Just cn
            }
    | otherwise = Nothing
  where
    isNullary (TDCon _ (TName "()")) = True
    isNullary _                      = False

    conName (TDCon c _) = T.pack c
    conName t           = error ("conName: " ++ show t)

refProperty :: String -> Property
refProperty n = emptyProp { propRef = Just (T.pack n) }

collProperty :: Type -> Property
collProperty (Type n (TDCon _ (TRecd _))) = emptyProp
    { propType  = Just "array"
    , propItems = Just $ ItemRef (T.pack n)
    }
collProperty (Type n (TDCon _ (TSum _))) = emptyProp
    { propType  = Just "array"
    , propItems = Just $ ItemRef (T.pack n)
    }
collProperty t = emptyProp
    { propType  = Just "array"
    , propItems = ItemType <$> typeName t
    }

prdProperty :: [Type] -> Maybe Property
prdProperty [] = Nothing
prdProperty ts
    | homogenous = Just $ emptyProp
        { propType = Just "array"
        , propItems = ItemType <$> typeName (head ts)
        }
    | otherwise = Nothing
  where
    homogenous = all (uncurry (==)) (zip ts (tail ts))

typeName :: Type -> Maybe Text
typeName (TName n)   = Just (mapName $ T.pack n)
typeName (Type  n _) = Just (T.pack n)
typeName _           = Nothing

mapName :: Text -> Text
mapName "Bool"    = "bool"
mapName "Int"     = "int"
mapName "Integer" = "integer"
mapName "Float"   = "float"
mapName "Double"  = "double"
mapName "Char"    = "char"
mapName "()"      = "void"
mapName txt       = txt

instance ToJSON Swagger where
    toJSON x = object
        [ "swaggerVersion" .= swgSwaggerVersion x
        , "apiVersion"     .= swgApiVersion x
        , "basePath"       .= swgBasePath x
        , "resourcePath"   .= swgResourcePath x
        , "produces"       .= swgProduces x
        , "apis"           .= toJSON (swgApis x)
        , "models"         .=
            let mods = map toJSON (swgModels x) in
            List.foldl' union emptyObject mods
        ]

instance ToJSON Api where
    toJSON x = object
        [ "path"       .= apiPath x
        , "operations" .= toJSON (apiOperations x)
        ]

instance ToJSON Operation where
    toJSON x = object
        [ "method"           .= opMethod x
        , "summary"          .= opSummary x
        , "notes"            .= opNotes x
        , "type"             .= opType x
        , "nickname"         .= opNickname x
        , "parameters"       .= opParameters x
        , "responseMessages" .= opResponses x
        ]

instance ToJSON Parameter where
    toJSON x = object
        [ "name"        .= parName x
        , "description" .= parDescription x
        , "required"    .= parRequired x
        , "type"        .= parType x
        , "paramType"   .= parParamType x
        ]

instance ToJSON Response where
    toJSON x = object
        [ "code"    .= respCode x
        , "message" .= respMessage x
        ]

instance ToJSON Model where
    toJSON x = object
        [ modName x .= object
            [ "id"         .= modId x
            , "required"   .= modRequired x
            , "properties" .=
                let props = map jsonProperty (modProperties x) in
                List.foldl' union emptyObject props
            ]
        ]
      where
        jsonProperty (n, p) = object [ n .= toJSON p ]

instance ToJSON Property where
    toJSON x = object
        [ "type"        .= propType x
        , "$ref"        .= propRef x
        , "format"      .= propFormat x
        , "description" .= propDescription x
        , "enum"        .= propEnum x
        , "minimum"     .= propMinimum x
        , "maximum"     .= propMaximum x
        ] `union` maybe Null toJSON (propItems x)

instance ToJSON Item where
    toJSON (ItemType t) = object [ "items" .= object [ "type" .= t ]]
    toJSON (ItemRef  r) = object [ "items" .= object [ "$ref" .= r ]]

union :: Value -> Value -> Value
union (Object a) (Object b) = Object (a <> b)
union (Array a)  (Array b)  = Array (a <> b)
union Null       b          = b
union a          _          = a
